import tkinter as tk
from tkinter import filedialog
import pygame
class MusicPlayer:
    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Music Player")        
        self.current_song = ""
        self.paused = False
        self.background_color = "#212121"
        self.foreground_color = "#FFFFFF"
        self.button_color = "#2962FF"        
        self.window.config(bg=self.background_color)        
        self.song_label = tk.Label(self.window, text="No song selected", bg=self.background_color, fg=self.foreground_color)
        self.song_label.pack(pady=20)        
        self.select_button = tk.Button(self.window, text="Select Song", command=self.select_song, bg=self.button_color, fg=self.foreground_color)
        self.select_button.pack(pady=10)        
        self.play_button = tk.Button(self.window, text="Play", state=tk.DISABLED, command=self.play_song, bg=self.button_color, fg=self.foreground_color)
        self.play_button.pack(pady=10)        
        self.pause_button = tk.Button(self.window, text="Pause", state=tk.DISABLED, command=self.pause_song, bg=self.button_color, fg=self.foreground_color)
        self.pause_button.pack(pady=10)        
        self.stop_button = tk.Button(self.window, text="Stop", state=tk.DISABLED, command=self.stop_song, bg=self.button_color, fg=self.foreground_color)
        self.stop_button.pack(pady=10)
        self.window.geometry("300x300")        
        self.window.mainloop()    
    def select_song(self):
        self.current_song = filedialog.askopenfilename(initialdir="/", title="Select Song", filetypes=(("MP3 Files", "*.mp3"),))
        if self.current_song:
            self.song_label["text"] = f"Selected song: {self.current_song}"
            self.play_button["state"] = tk.NORMAL    
    def play_song(self):
        if self.current_song:
            if self.paused:
                pygame.mixer.music.unpause()
                self.paused = False
            else:
                pygame.mixer.init()
                pygame.mixer.music.load(self.current_song)
                pygame.mixer.music.play()                
            self.pause_button["state"] = tk.NORMAL
            self.stop_button["state"] = tk.NORMAL
            self.play_button["state"] = tk.DISABLED    
    def pause_song(self):
        if self.current_song and pygame.mixer.music.get_busy():
            pygame.mixer.music.pause()
            self.paused = True
            self.pause_button["state"] = tk.DISABLED
            self.play_button["state"] = tk.NORMAL    
    def stop_song(self):
        if self.current_song and pygame.mixer.music.get_busy():
            pygame.mixer.music.stop()
            self.paused = False
            self.pause_button["state"] = tk.DISABLED
            self.stop_button["state"] = tk.DISABLED
            self.play_button["state"] = tk.NORMAL
MusicPlayer()
