from tkinter import *
import ctypes
import random
import tkinter
ctypes.windll.shcore.SetProcessDpiAwareness(1)
storage = Tk()
storage.title('CopyAssignment - Typing Speed Test')
storage.geometry('1400x700')
storage.option_add("*Label.Font", "consolas 30")
storage.option_add("*Button.Font", "consolas 30")
def handling_labels():
    random_selection = [
        'A paragraph is a collection of words strung together to make a longer unit than a sentence. Several sentences often make a paragraph. There are normally three to eight sentences in a paragraph. Paragraphs can start with a five-space indentation or by skipping a line and then starting over. This makes it simpler to tell when one paragraph ends and the next starts simply it has 3-9 lines.',
        'This Is Typing Test Program You Can Type And You Can Get One Minute Per Minute',
        'This Is The Program To Check Your Speed Test'
    ]    
    text = random.choice(random_selection).lower()
    split_point = 0    
    global name_label_left
    name_label_left = Label(storage, text=text[0:split_point], fg='green')
    name_label_left.place(relx=0.5, rely=0.5, anchor=E)    
    global name_label_right
    name_label_right = Label(storage, text=text[split_point:])
    name_label_right.place(relx=0.5, rely=0.5, anchor=W)    
    global current_alphabet_label
    current_alphabet_label = Label(storage, text=text[split_point], fg='grey')
    current_alphabet_label.place(relx=0.5, rely=0.6, anchor=N)    
    global seconds_left
    heading_label = Label(storage, text=f'CopyAssignment - Typing Speed Test', fg='blue')
    heading_label.place(relx=0.5, rely=0.2, anchor=S)    
    seconds_left = Label(storage, text=f'0 Seconds', fg='red')
    seconds_left.place(relx=0.5, rely=0.4, anchor=S)    
    global writable
    writable = True
    storage.bind('<Key>', handle_key_press)    
    global seconds_passed
    seconds_passed = 0
    storage.after(60000, stop_game)
    storage.after(1000, time_addition)
def stop_game():
    global writable
    writable = False
    amount_words = len(name_label_left.cget('text').split(' '))
    seconds_left.destroy()
    current_alphabet_label.destroy()
    name_label_right.destroy()
    name_label_left.destroy()    
    global result_label
    result_label = Label(storage, text=f'Words per Minute (WPM): {amount_words}', fg='black')
    result_label.place(relx=0.5, rely=0.4, anchor=CENTER)    
    global results_button
    results_button = Button(storage, text=f'Retry', command=restart_game)
    results_button.place(relx=0.5, rely=0.6, anchor=CENTER)
def restart_game():
    result_label.destroy()
    results_button.destroy()
    handling_labels()
def time_addition():
    global seconds_passed
    seconds_passed += 1
    seconds_left.configure(text=f'{seconds_passed} Seconds')
    if writable:
        storage.after(1000, time_addition)
def handle_key_press(event=None):
    try:
        if event.char.lower() == name_label_right.cget('text')[0].lower():
            name_label_right.configure(text=name_label_right.cget('text')[1:])
            name_label_left.configure(text=name_label_left.cget('text') + event.char.lower())
            current_alphabet_label.configure(text=name_label_right.cget('text')[0])
    except tkinter.TclError:
        pass
handling_labels()
storage.mainloop()
